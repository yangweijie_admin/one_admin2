# 如何实现tp6 海豚


## 验证码

composer require topthink/think-captcha


## 多应用

composer require topthink/think-multi-app

php think build admin
php think build common

`php extend/vqmod/install/index.php`



## 事件

event.php HttpRun 添加

~~~
<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [
            'app\\common\\listener\\Config',
            'app\\common\\listener\\Hook',
        ],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
    ],

    'subscribe' => [
    ],
];

~~~

