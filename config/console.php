<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
		'download_playlist'=>'app\music\command\DownloadPlayList',
		'mv:build' => 'app\music\command\MvBuild',
		'playlist_init'=>'app\music\command\PlaylistInit',
    ],
];
